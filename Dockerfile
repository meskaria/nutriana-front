#We will use the node image from dockerhub, we dont want to waste time setting up node.
FROM node:8.9.1

#We create a forder for our app
RUN mkdir /app

#We copy the contet of our whole project to the docker containers app folder
ADD . /app

#We go to the app dir (similar to "cd /app")
WORKDIR /app

#We install the npm dependecies of the project (based on package.json)
RUN yarn install

#We compile an optimized version of our app. (AOT && Treeshaking)
RUN yarn run build

#We install a http-server which is a lightweight solution for serving the compiled files
RUN yarn global add http-server

#We expose port 80 (which is more like documentation)
EXPOSE 80

#We go to /app/dist where the compiled files are.
WORKDIR /app/build

#When we run the dockerimage it will spin up the http-server on port 80
CMD ["http-server", "-p 80"]
